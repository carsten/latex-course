\documentclass[handout]{latexkurs}

\subtitle{Grafiken erstellen mit \LaTeX{}}
\date{\dateSeventhLecture}

\def\TikZ{Ti\emph{k}Z}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Grafiken erstellen in \LaTeX}

  \onslide<+->

  \LaTeX\ stellt von sich aus Möglichkeiten bereit, Graphiken zu erzeugen:
  \begin{itemize}
  \item<2-> die \texttt{picture}-Umgebung
  \item<2-> \texttt{pict2e}, um die \texttt{picture}-Umgebung zu erweitern
  \item<2-> \texttt{epic}, \textit{enhanced picture}
  \item<2-> \texttt{eepic}, \textit{enhanced epic}
  \item<2-> \texttt{pmgraph}, \textit{poor man graphics}
  \item<3-> \alert<3-5>{pstricks}
    \begin{itemize}
    \item<4-> schnell (nutzt Postscript)
    \item<5-> funktioniert nicht mit \texttt{pdftex} und \texttt{luatex}
    \end{itemize}
  \item<6-> \alert<6->{\TikZ}
    \begin{itemize}
    \item<7-> portabel
    \item<8-> langsamer als \texttt{pstricks}
    \item<9-> subjektiv einfacher als \texttt{pstricks}
    \end{itemize}
  \end{itemize}

\end{frame}

\begin{luacode}
  function weierstrass(x0, x1, n, a, b, epsilon)
    local dx = (x1 - x0) / n
    local x = x0
    local out = assert(io.open("tmp.data", "w"))
    local y, k, dy

    while (x <= x1) do
      y = 0
      k = 0
      repeat
        dy = math.pow(a, k) * math.cos(math.pow(b, k) * math.pi * x)
        y = y + dy
        k = k + 1
      until (math.abs(dy) < epsilon)
      out:write(x, " ", y, "\string\n")
      x = x + dx
    end
    out:close()
  end
\end{luacode}

\directlua{weierstrass(-2,2,500,0.3,5,1.e-12)}

\begin{frame}
  \frametitle{Weierstraß-Funktion mit \TikZ\ und Lua}

  \begin{equation*}
    x \mapsto \sum_{n=0} ^\infty a^n \cos(b^n \pi x)
  \end{equation*}

  \begin{center}
    \begin{tikzpicture}
      \begin{axis}[axis lines=middle, ymin=-1.5, ymax=1.75]
        \addplot[thin, blue] table {tmp.data};
      \end{axis}
    \end{tikzpicture}
  \end{center}

\end{frame}


\section{Grundlagen von \TikZ}

\begin{frame}[fragile]
  \frametitle{Wie? }

  \onslide<+->

  Um \TikZ\ in \LaTeX\ zu nutzen:
  \begin{itemize}
  \item<+-> \lstinline!\usepackage{tikz}!
  \item<+-> Aller Code zwischen \lstinline!\begin{tikzpicture}! und
      \lstinline!\end{tikzpicture}! wird von \LaTeX\ an \TikZ\
    abgegeben.
  \end{itemize}

  \onslide<+->

  \begin{block}{\textcolor{red}{\textbf{ACHTUNG!}}}
    \TikZ\ ist eine eigene \enquote{Sprache}!
  \end{block}

\end{frame}

\begin{frame}[fragile,t]
  \frametitle{Die \texttt{tikzpicture} Umgebung.}

\begin{lstlisting}
\begin{tikzpicture}
  CODE
\end{tikzpicture}
\end{lstlisting}
\pause
Kann zum Beispiel in \texttt{figure} Umgebung eingebettet werden.
\begin{lstlisting}
\begin{figure}[h]
  \centering
  \begin{tikzpicture}
    CODE
  \end{tikzpicture}
  \caption{\label{fig:someref} some caption}
\end{figure}
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Ein paar Linien …}

  \centering

  \pause

  \begin{columns}
    \begin{column}{0.3\linewidth}
      \centering
      \tikzexternalenable
      \begin{tikzpicture}[step=0.3]
        \draw<2->[red] (0,0) -- (2,3);
        \draw<3->[thick,blue] (1,1) -- (1,3);
        \uncover<4->{\draw[step=0.3] (0,0) grid (3,3);}
      \end{tikzpicture}
      \tikzexternaldisable
    \end{column}
    \begin{column}{0.7\linewidth}
\begin{lstlisting}[frame=none]
\begin{tikzpicture}
 \draw[red] (0,0) -- (2,3); \pause
 \draw[thick,blue] (1,1) -- (1,3); \pause
 \draw[step=.3] (0,0) grid (3,3);
\end{tikzpicture}
\end{lstlisting}
    \end{column}
  \end{columns}

  \pause

  \medskip

  \begin{columns}
    \begin{column}{0.3\linewidth}
      \centering
      \tikzexternalenable
      \begin{tikzpicture}[step=0.3]
        \draw<5->[red,->] (0,0) -- (2,3);
        \draw<6->[blue,dashed] (1,1) -- (1,3);
        \uncover<7->{\draw[dotted] (0,0) grid (3,3);}
      \end{tikzpicture}
      \tikzexternaldisable
    \end{column}
    \begin{column}{0.7\linewidth}
\begin{lstlisting}[frame=none]
\begin{tikzpicture}[step=0.3]
 \draw[red,->] (0,0) -- (2,3); \pause
 \draw[blue,dashed] (1,1) -- (1,3); \pause
 \draw[dotted] (0,0) grid (3,3);
\end{tikzpicture}
\end{lstlisting}
    \end{column}
  \end{columns}

\end{frame}


\begin{frame}[fragile,t]
  \frametitle{Noch mehr Linien …}

  \pause

  \begin{columns}
    \begin{column}{0.3\linewidth}
      \centering
      \tikzexternalenable
      \begin{tikzpicture}[step=0.4,scale=0.8]
        \uncover<2->{\draw[red!50,thick,<<-] (0,0) -- (2,3);}
        \draw<3->[line width=4pt,blue] (1,1) -- (0,2);
        \uncover<4->{\draw[dotted] (0,0) grid (3,3);}
      \end{tikzpicture}
      \tikzexternaldisable
    \end{column}
    \begin{column}{0.7\linewidth}
\begin{lstlisting}[frame=none]
\begin{tikzpicture}[scale=0.8,step=0.4]
 \draw[red!50,thick,<<-] (0,0) -- (2,3); \pause
 \draw[line width=4pt,blue] (1,1) -- (0,2); \pause
 \draw[dotted] (0,0) grid (3,3);
\end{tikzpicture}
\end{lstlisting}
    \end{column}
  \end{columns}

  \pause

  \medskip

  \begin{columns}
    \begin{column}{0.3\linewidth}
      \centering
      \tikzexternalenable
      \begin{tikzpicture}[every node/.style={below}]
        \tikzset{step=0.3}
        \draw<5->[red] (0,0) -- (2,3) node[above] {a};
        \draw<6->[blue,dashed] (1,1) rectangle (2,2);
        \uncover<7->{\draw[dotted] (0,0) node {0} grid (3,3);}
        \draw<8->[fill,,opacity=0.3] (1,1) circle(.707);
      \end{tikzpicture}
      \tikzexternaldisable
    \end{column}
    \begin{column}{0.7\linewidth}
\begin{lstlisting}[frame=none]
\begin{tikzpicture}[step=0.3]
  \tikzset{every node/.style={below}}
  \draw[red] (0,0) -- (2,3) node[above] {a}; \pause
  \draw[blue,dashed] (1,1) rectangle (2,2); \pause
  \draw[dotted] (0,0) node {0} grid (3,3); \pause
  \draw[fill,opacity=0.3] (1,1) circle(1);
\end{tikzpicture}
\end{lstlisting}
    \end{column}
  \end{columns}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Jetzt wird's kurvig!}

  \pause

  \begin{columns}
    \begin{column}{0.3\linewidth}
      \centering
      \tikzexternalenable
      \begin{tikzpicture}
        \coordinate (a) at (0,0);
        \coordinate (b) at (0,1);
        \coordinate (c) at (1,1);
        \coordinate (d) at (3,2);
        \draw[blue] (a) -- (d);
        \draw<2->  (a)..controls (b) and (c)..(d);
        \draw<3->[green,bend left=30] (a) to (d);
        \draw<4->[red,out=90, in=-90] (a) to (d);
        \draw<5-> ($(a)!0.5!(d)$) ellipse (1 and 0.5);
      \end{tikzpicture}
      \tikzexternaldisable
    \end{column}
    \begin{column}{0.7\linewidth}
\begin{lstlisting}[mathescape=false,frame=none]
\usetikzlibrary{calc}
\begin{tikzpicture}
  \coordinate (a) at (0,0);
  \coordinate (b) at (0,1);
  \coordinate (c) at (1,1);
  \coordinate (d) at (3,2);
  \draw[blue] (a) -- (d);
  \draw  (a)..controls (b) and (c)..(d); \pause
  \draw[green,bend left=30] (a) to (d); \pause
  \draw[red,out=90, in=-90] (a) to (d); \pause
  \draw ($(a)!0.5!(d)$) ellipse(1 and 0.5);
\end{tikzpicture}
\end{lstlisting}
    \end{column}
  \end{columns}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Zickzack}

  \pause

  \begin{columns}
    \begin{column}{0.3\linewidth}
      \centering
      \begin{tikzpicture}
        \coordinate (a) at (0,0);
        \coordinate (b) at (1,1);
        \coordinate (c) at (3,2);
        \draw<2->[blue] (a) -- (c);
        \draw<3->  (a)--(b)--(c);
        \begin{scope}[yshift=1cm,dashed,thick]
          \coordinate (a) at (0,0);
          \coordinate (c) at (3,2);
          \uncover<6->{\draw[red]  (a)--(b)--(c);}
          \node<7->[draw,circle through=(a)] at (b){};
        \end{scope}
      \end{tikzpicture}
    \end{column}
    \begin{column}{0.7\linewidth}
\begin{lstlisting}[mathescape=false,frame=none]
\usetikzlibrary{through}
\begin{tikzpicture}
  \coordinate (a) at (0,0);
  \coordinate (b) at (1,1);
  \coordinate (c) at (3,2);
  \draw[blue] (a) -- (c);\pause
  \draw (a)--(b)--(c);\pause
  \begin{scope}[yshift=1cm,dashed,thick] \pause
    \coordinate (a) at (0,0);
    \coordinate (c) at (3,2);\pause
    \draw[red] (a)--(b)--(c);\pause
    \node[draw,circle through=(a)] at (b){};
  \end{scope}
\end{tikzpicture}
\end{lstlisting}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Eine andere Sicht.}

  \pause

  \begin{columns}
    \begin{column}{0.2\linewidth}
      \centering
      \begin{tikzpicture}
        \tikzset{tomsbox/.style={rectangle, draw, very thick,
            minimum size=7mm, rounded corners=2mm}}
        \node[tomsbox] (1) at (0,0) {$T_1$};
        \node[tomsbox] (2) [below=of 1] {$T_2$};
        \node[tomsbox] (3) [below=of 2] {$T_3$};
        \node[tomsbox] (4) [below=of 3] {$T_4$};
        \draw (1)--(2)--(3)--(4);
      \end{tikzpicture}
    \end{column}
  \begin{column}{0.8\linewidth}
\begin{lstlisting}[mathescape=false,frame=none]
\usetikzlibrary{positioning} % in der Präambel
\begin{tikzpicture}
  \tikzset{tomsbox/.style={rectangle, draw,
      very thick,minimum size=7mm,
      rounded corners=2mm}}
  \node[tomsbox] (1) at (0,0) {$T_1$};
  \node[tomsbox] (2) [below=of 1] {$T_2$};
  \node[tomsbox] (3) [below=of 2] {$T_3$};
  \node[tomsbox] (4) [below=of 3] {$T_4$};
  \draw (1)--(2)--(3)--(4);
\end{tikzpicture}
\end{lstlisting}
  \end{column}
\end{columns}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Wiederholungen}

  \pause

  \begin{columns}
    \begin{column}{0.2\linewidth}
      \centering
      \begin{tikzpicture}
        \tikzset{tomsbox/.style={rectangle, draw, very thick,
            minimum size=7mm, rounded corners=2mm}}
        \node[tomsbox] (1) at (0,0) {$T_1$};
        \foreach \i [count=\j] in {2,3,4}{
          \node[tomsbox] (\i) [below=of \j] {$T_\i$};
          \draw (\j)--(\i);
        }
      \end{tikzpicture}
    \end{column}
    \begin{column}{0.82\linewidth}
\begin{lstlisting}[mathescape=false,frame=none]
\begin{tikzpicture}
  \tikzset{tomsbox/.style={rectangle, draw,
      very thick,minimum size=7mm,
      rounded corners=2mm}}
  \node[tomsbox] (1) at (0,0) {$T_1$};
  \foreach \i [count=\j] in {2,3,4}
  {
    \node[tomsbox] (\i) [below=of \j] {$T_\i$};
    \draw (\j)--(\i);
  }
\end{tikzpicture}
\end{lstlisting}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Bibliotheken}

  \onslide<+->

  Die Funktionalität von \TikZ\ kann durch externe \emph{Bibliotheken} erweitert werden.

  \onslide<+->

  \begin{Beispiel}
\begin{lstlisting}
\usetikzlibrary{automata}
\end{lstlisting}

    \centering
    \begin{tikzpicture}[shorten >=1pt,node distance=1cm and 3cm,on grid,
      every state/.style={draw=blue!50,very thick,fill=blue!20}]
      \node[state,initial] (q_0)                      {$q_0$};
      \node[state]         (q_1) [above right=of q_0] {$q_1$};
      \node[state]         (q_2) [below right=of q_0] {$q_2$};
      \path[->] (q_0) edge              node [above left] {0} (q_1)
                      edge              node [below left] {1} (q_2)
                (q_1) edge [loop above] node              {0} ()
                (q_2) edge [loop below] node              {1} ();
    \end{tikzpicture}
  \end{Beispiel}

\end{frame}

\begin{frame}
  \frametitle{Bibliotheken}

  \onslide<+->

  \texttt{3d},
  \texttt{angles},
  \texttt{arrows},
  \texttt{automata},
  \texttt{babel},
  \texttt{backgrounds},
  \texttt{bending},
  \texttt{calc},
  \texttt{calendar},
  \texttt{chains},
  \texttt{decorations},
  \texttt{decorations.footprints},
  \texttt{decorations.fractals},
  \texttt{decorations.markings},
  \texttt{decorations.pathmorphing},
  \texttt{decorations.pathreplacing},
  \texttt{decorations.shapes},
  \texttt{decorations.text},
  \texttt{er},
  \texttt{fadings},
  \texttt{fit},
  \texttt{fixedpointarithmetic},
  \texttt{folding},
  \texttt{fpu},
  \texttt{intersections},
  \texttt{lindenmayersystems},
  \texttt{math},
  \texttt{matrix},
  \texttt{mindmap},
  \texttt{patterns},
  \texttt{petri},
  \texttt{plothandlers},
  \texttt{plotmarks},
  \texttt{positioning},
  \texttt{quotes},
  \texttt{scopes},
  \texttt{shadings},
  \texttt{shadows},
  \texttt{shapes.arrows},
  \texttt{shapes.callouts},
  \texttt{shapes},
  \texttt{shapes.gates.logic.IEC},
  \texttt{shapes.gates.logic.US},
  \texttt{shapes.geometric},
  \texttt{shapes.misc},
  \texttt{shapes.multipart},
  \texttt{shapes.symbols},
  \texttt{snakes},
  \texttt{spy},
  \texttt{svg.path},
  \texttt{through},
  \texttt{topaths},
  \texttt{trees},
  \texttt{turtle}

\end{frame}

\section{Datenvisualisierung}

\begin{frame}[fragile]
  \frametitle{Datenvisualisierung}
  \onslide<+->

  \begin{columns}
    \begin{column}{0.3\linewidth}
      \centering
      \begin{tikzpicture}
        \datavisualization [school book axes, visualize as smooth line]
          data {x, y
            -1.5, 2.25
            -1, 1
            -.5, .25
            0, 0
            .5, .25
            1, 1
            1.5, 2.25
          };
      \end{tikzpicture}
    \end{column}
    \begin{column}{0.6\linewidth}
      \small
\begin{lstlisting}[frame=none]
\usetikzlibrary{datavisualization}
\begin{tikzpicture}
  \datavisualization [school book axes,
    visualize as smooth line]
    data {x, y
      -1.5, 2.25
      -1, 1
      -.5, .25
      0, 0
      .5, .25
      1, 1
      1.5, 2.25
    };
\end{tikzpicture}
\end{lstlisting}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Automatisch}

  \onslide<+->

  \begin{columns}
    \begin{column}{0.3\linewidth}
      \centering
      \begin{tikzpicture}[baseline]
        \datavisualization [school book axes,
        visualize as smooth line]
        data [format=function] {
          var x : interval [-1.5:1.5] samples 7;
          func y = \value x * \value x;
        };
      \end{tikzpicture}
    \end{column}
    \begin{column}{0.6\linewidth}
      \small
\begin{lstlisting}[mathescape=false,frame=none]
\usetikzlibrary{datavisualization,
  datavisualization.formats.functions}
\begin{tikzpicture}
  \datavisualization
  [school book axes,
   visualize as smooth line]
  data [format=function] {
    var x : interval [-1.5:1.5]
      samples 7;
    func y = \value x * \value x;
  };
\end{tikzpicture}
      \end{lstlisting}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Achsen}

  \onslide<+->

  \begin{columns}
    \begin{column}{0.3\linewidth}
      \centering
      \begin{tikzpicture}[baseline]
        \datavisualization [scientific axes=clean,
        x axis={length=.7\linewidth, ticks=few},
        visualize as smooth line]
        data [format=function] {
          var x : interval [-1.5:1.5] samples 7;
          func y = \value x * \value x;
        };
      \end{tikzpicture}
    \end{column}
    \begin{column}{0.6\linewidth}
      \small
\begin{lstlisting}[mathescape=false,frame=none]
\begin{tikzpicture}
  \datavisualization
  [scientific axes=clean,
   x axis={length=.7\linewidth,
     ticks=few},
   visualize as smooth line]
  data [format=function] {
    var x : interval [-1.5:1.5]
      samples 7;
    func y = \value x * \value x;
  };
\end{tikzpicture}
      \end{lstlisting}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Darstellung}

  \onslide<+->

  \begin{columns}
    \begin{column}{0.3\linewidth}
      \centering
      \begin{tikzpicture}[baseline]
        \datavisualization [scientific axes=clean,
        x axis={length=.7\linewidth, ticks=few},
        visualize as scatter]
        data [format=function] {
          var x : interval [-1.5:1.5] samples 7;
          func y = \value x * \value x;
        };
      \end{tikzpicture}
    \end{column}
    \begin{column}{0.6\linewidth}
      \small
\begin{lstlisting}[mathescape=false,frame=none]
\begin{tikzpicture}
  \datavisualization
  [scientific axes=clean,
   x axis={length=.7\linewidth,
     ticks=few},
   visualize as scatter]
  data [format=function] {
    var x : interval [-1.5:1.5]
      samples 7;
    func y = \value x * \value x;
  };
\end{tikzpicture}
\end{lstlisting}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Legenden}

  \onslide<+->

  \begin{columns}
    \begin{column}{0.3\linewidth}
      \centering
      \begin{tikzpicture}[baseline]
        \datavisualization [scientific axes=clean,
        x axis={length=.7\linewidth, ticks=few},
        legend={below},
        visualize as smooth line/.list={square},
        square={label in legend={text=$x^2$}}]
        data [format=function,set=square] {
          var x : interval [-1.5:1.5] samples 7;
          func y = \value x * \value x;
        };
      \end{tikzpicture}
    \end{column}
    \begin{column}{0.6\linewidth}
      \small
\begin{lstlisting}[mathescape=false,frame=none]
\begin{tikzpicture}
  \datavisualization
  [scientific axes=clean,
   x axis={length=.7\linewidth,
     ticks=few},
   legend={below},
   visualize as smooth
     line/.list={sq},
   sq={label in legend={text=$x^2$}}]
  data [format=function,set=sq] {
    var x : interval [-1.5:1.5]
      samples 7;
    func y = \value x * \value x;
  };
\end{tikzpicture}
\end{lstlisting}
    \end{column}
  \end{columns}
\end{frame}

\mode<handout>

\section{Ein Tutorial}

\begin{frame}[t]

  \begin{block}{Ziel}
    \begin{center}
      \begin{tikzpicture}[
        thick,
        >=stealth',
        dot/.style = {
          draw,
          fill = white,
          circle,
          inner sep = 0pt,
          minimum size = 4pt
        }
        ]
        \coordinate (O) at (0,0);
        \draw[->] (-0.3,0) -- (8,0) coordinate[label = {below:$x$}] (xmax);
        \draw[->] (0,-0.3) -- (0,5) coordinate[label = {right:$f(x)$}] (ymax);
        \path[name path=x] (0.3,0.5) -- (6.7,4.7);
        \path[name path=y] plot[smooth] coordinates {(-0.3,2) (2,1.5) (4,2.8) (6,5)};
        \begin{scope}[name intersections = {of = x and y, name = i}]
          \fill[gray!20] (i-1) -- (i-2 |- i-1) -- (i-2) -- cycle;
          \draw      (0.3,0.5) -- (6.7,4.7) node[pos=0.8, below right] {Sekante};
          \draw[red] plot[smooth] coordinates {(-0.3,2) (2,1.5) (4,2.8) (6,5)};
          \draw (i-1) node[dot, label = {above:$P$}] (i-1) {} -- node[left,yshift=-3pt]
          {$f(x_0)$} (i-1 |- O) node[dot, label = {below:$x_0$}] {};
          \path (i-2) node[dot, label = {above:$Q$}] (i-2) {} -- (i-2 |- i-1)
          node[dot] (i-12) {};
          \draw           (i-12) -- (i-12 |- O) node[dot,
          label = {below:$x_0 + \varepsilon$}] {};
          \draw[blue, <->] (i-2) -- node[right] {$f(x_0 + \varepsilon) - f(x_0)$}
          (i-12);
          \draw[blue, <->] (i-1) -- node[below] {$\varepsilon$} (i-12);
          \path       (i-1 |- O) -- node[below] {$\varepsilon$} (i-2 |- O);
          \draw[gray]      (i-2) -- (i-2 -| xmax);
          \draw[gray, <->] ([xshift = -0.5cm]i-2 -| xmax) -- node[fill = white]
          {$f(x_0 + \varepsilon)$}  ([xshift = -0.5cm]xmax);
        \end{scope}
      \end{tikzpicture}
    \end{center}
  \end{block}

\end{frame}

\begin{frame}[t,fragile]

  \onslide<+->

  \begin{center}
    \scalebox{0.5}{
      \begin{tikzpicture}
        \coordinate (O) at (0,0); \draw[->] (-0.3,0) -- (8,0) coordinate[label =
        {below:$x$}] (xmax); \draw[->] (0,-0.3) -- (0,5) coordinate[label =
        {right:$f(x)$}] (ymax);
      \end{tikzpicture}
    }
  \end{center}

  \small

\begin{lstlisting}
\coordinate (O) at (0,0);
\draw[->] (-0.3,0) -- (8,0) coordinate[label = {below:$x$}] (xmax);
\draw[->] (0,-0.3) -- (0,5) coordinate[label = {right:$f(x)$}] (ymax);
\end{lstlisting}

\end{frame}

\begin{frame}[t,fragile]

  \begin{center}
    \scalebox{0.5}{
      \begin{tikzpicture}
        \coordinate (O) at (0,0);
        \draw[->] (-0.3,0) -- (8,0) coordinate[label = {below:$x$}] (xmax);
        \draw[->] (0,-0.3) -- (0,5) coordinate[label = {right:$f(x)$}] (ymax);
        \draw      (0.3,0.5) -- (6.7,4.7) node[pos=0.8, below right] {Sekante};
        \draw[red] plot[smooth] coordinates {(-0.3,2) (2,1.5) (4,2.8) (6,5)};
      \end{tikzpicture}
    }
  \end{center}

  \small

\begin{lstlisting}
\draw (0.3,0.5) -- (6.7,4.7) node[pos=0.8, below right] {Sekante};
\draw[red] plot[smooth] coordinates {(-0.3,2) (2,1.5) (4,2.8) (6,5)};
\end{lstlisting}

\end{frame}

\begin{frame}[t,fragile]

  \onslide<+->

  \begin{center}
    \scalebox{0.5}{
      \begin{tikzpicture}
        \coordinate (O) at (0,0);
        \draw[->] (-0.3,0) -- (8,0) coordinate[label = {below:$x$}] (xmax);
        \draw[->] (0,-0.3) -- (0,5) coordinate[label = {right:$f(x)$}] (ymax);
        \path[name path=x] (0.3,0.5) -- (6.7,4.7);
        \path[name path=y] plot[smooth] coordinates {(-0.3,2) (2,1.5) (4,2.8) (6,5)};
        \begin{scope}[name intersections = {of = x and y, name = i}]
          \fill[gray!20] (i-1) -- (i-2 |- i-1) -- (i-2) -- cycle;
          \draw      (0.3,0.5) -- (6.7,4.7) node[pos=0.8, below right] {Sekante};
          \draw[red] plot[smooth] coordinates {(-0.3,2) (2,1.5) (4,2.8) (6,5)};
        \end{scope}
      \end{tikzpicture}
    }
  \end{center}

  \small

\begin{lstlisting}
\usetikzlibrary{intersections} % in der Präambel
\path[name path=x] (0.3,0.5) -- (6.7,4.7);
\path[name path=y] plot[smooth]
  coordinates {(-0.3,2) (2,1.5) (4,2.8) (6,5)};
\begin{scope}[name intersections = {of = x and y, name = i}]
  \fill[gray!20] (i-1) -- (i-2 |- i-1) -- (i-2) -- cycle;
  \draw (0.3,0.5) -- (6.7,4.7) node[pos=0.8, below right] {Sekante};
  \draw[red] plot[smooth]
    coordinates {(-0.3,2) (2,1.5) (4,2.8) (6,5)};
\end{scope}
\end{lstlisting}

\end{frame}

\begin{frame}[t,fragile]

  \begin{center}
    \scalebox{0.5}{
      \begin{tikzpicture}
        \coordinate (O) at (0,0);
        \draw[->] (-0.3,0) -- (8,0) coordinate[label = {below:$x$}] (xmax);
        \draw[->] (0,-0.3) -- (0,5) coordinate[label = {right:$f(x)$}] (ymax);
        \path[name path=x] (0.3,0.5) -- (6.7,4.7);
        \path[name path=y] plot[smooth] coordinates {(-0.3,2) (2,1.5) (4,2.8) (6,5)};
        \begin{scope}[name intersections = {of = x and y, name = i}]
          \fill[gray!20] (i-1) -- (i-2 |- i-1) -- (i-2) -- cycle;
          \draw      (0.3,0.5) -- (6.7,4.7) node[pos=0.8, below right] {Sekante};
          \draw[red] plot[smooth] coordinates {(-0.3,2) (2,1.5) (4,2.8) (6,5)};
          \draw (i-1) node[label = {above:$P$}] {} -- node[left,yshift=-3pt]
            {$f(x_0)$} (i-1 |- O) node[label = {below:$x_0$}] {};
        \end{scope}
      \end{tikzpicture}
    }
  \end{center}

  \small

\begin{lstlisting}
\begin{scope}[name intersections = {$\dots$}]
  $\dots\quad (\textsl{Wie vorher})$
  \draw (i-1) node[label = {above:$\text{\$P\$}$}] {}
    -- node[left,yshift=-3pt] {$\text{\$f(x\_0)\$}$}
    (i-1 |- O) node[label = {below:$\text{\$x\_0\$}$}] {};
\end{scope}
\end{lstlisting}

\end{frame}

\begin{frame}[t,fragile]

  \onslide<+->

  \begin{center}
    \scalebox{0.5}{
      \begin{tikzpicture}
        \coordinate (O) at (0,0);
        \draw[->] (-0.3,0) -- (8,0) coordinate[label = {below:$x$}] (xmax);
        \draw[->] (0,-0.3) -- (0,5) coordinate[label = {right:$f(x)$}] (ymax);
        \path[name path=x] (0.3,0.5) -- (6.7,4.7);
        \path[name path=y] plot[smooth] coordinates {(-0.3,2) (2,1.5) (4,2.8) (6,5)};
        \begin{scope}[name intersections = {of = x and y, name = i}]
          \fill[gray!20] (i-1) -- (i-2 |- i-1) -- (i-2) -- cycle;
          \draw      (0.3,0.5) -- (6.7,4.7) node[pos=0.8, below right] {Sekante};
          \draw[red] plot[smooth] coordinates {(-0.3,2) (2,1.5) (4,2.8) (6,5)};
          \draw (i-1) node[label = {above:$P$}] {} -- node[left,yshift=-3pt]
          {$f(x_0)$} (i-1 |- O) node[label = {below:$x_0$}] {};
          \path (i-2) node[label = {above:$Q$}] {} -- (i-2 |- i-1) node (i-12) {};
        \end{scope}
      \end{tikzpicture}
    }
  \end{center}

  \small

\begin{lstlisting}
\begin{scope}[$\dots$]
  $\dots\quad (\textsl{Wie vorher})$
  \path (i-2) node[label = {above:$\text{\$Q\$}$}] {}
    -- (i-2 |- i-1) node (i-12) {};
\end{scope}
\end{lstlisting}

\end{frame}

\begin{frame}[t,fragile]

  \begin{center}
    \scalebox{0.5}{
      \begin{tikzpicture}
        \coordinate (O) at (0,0);
        \draw[->] (-0.3,0) -- (8,0) coordinate[label = {below:$x$}] (xmax);
        \draw[->] (0,-0.3) -- (0,5) coordinate[label = {right:$f(x)$}] (ymax);
        \path[name path=x] (0.3,0.5) -- (6.7,4.7);
        \path[name path=y] plot[smooth] coordinates {(-0.3,2) (2,1.5) (4,2.8) (6,5)};
        \begin{scope}[name intersections = {of = x and y, name = i}]
          \fill[gray!20] (i-1) -- (i-2 |- i-1) -- (i-2) -- cycle;
          \draw      (0.3,0.5) -- (6.7,4.7) node[pos=0.8, below right] {Sekante};
          \draw[red] plot[smooth] coordinates {(-0.3,2) (2,1.5) (4,2.8) (6,5)};
          \draw (i-1) node[label = {above:$P$}] {} -- node[left,yshift=-3pt]
          {$f(x_0)$} (i-1 |- O) node[label = {below:$x_0$}] {};
          \path (i-2) node[label = {above:$Q$}] {} -- (i-2 |- i-1) node (i-12) {};
          \draw (i-12) -- (i-12 |- O) node[label = {below:$x_0 + \varepsilon$}] {};
        \end{scope}
      \end{tikzpicture}
    }
  \end{center}

  \small

\begin{lstlisting}
\begin{scope}[$\dots$]
  $\dots\quad(\textsf{Wie vorher})$
  \draw (i-12) -- (i-12 |- O)
   node[label = {below:$\text{\${}x\_0 + \textbackslash{}varepsilon\$}$}] {};
\end{scope}
\end{lstlisting}

\end{frame}

\begin{frame}[t,fragile]

  \begin{center}
    \scalebox{0.5}{
      \begin{tikzpicture}
        \coordinate (O) at (0,0);
        \draw[->] (-0.3,0) -- (8,0) coordinate[label = {below:$x$}] (xmax);
        \draw[->] (0,-0.3) -- (0,5) coordinate[label = {right:$f(x)$}] (ymax);
        \path[name path=x] (0.3,0.5) -- (6.7,4.7);
        \path[name path=y] plot[smooth] coordinates {(-0.3,2) (2,1.5) (4,2.8) (6,5)};
        \begin{scope}[name intersections = {of = x and y, name = i}]
          \fill[gray!20] (i-1) -- (i-2 |- i-1) -- (i-2) -- cycle;
          \draw      (0.3,0.5) -- (6.7,4.7) node[pos=0.8, below right] {Sekante};
          \draw[red] plot[smooth] coordinates {(-0.3,2) (2,1.5) (4,2.8) (6,5)};
          \draw (i-1) node[label = {above:$P$}] {} -- node[left,yshift=-3pt]
          {$f(x_0)$} (i-1 |- O) node[label = {below:$x_0$}] {};
          \path (i-2) node[label = {above:$Q$}] {} -- (i-2 |- i-1) node (i-12) {};
          \draw (i-12) -- (i-12 |- O) node[label = {below:$x_0 + \varepsilon$}] {};
          \draw[blue, <->] (i-2) -- node[right] {$f(x_0 + \varepsilon) - f(x_0)$} (i-12);
          \draw[blue, <->] (i-1) -- node[below] {$\varepsilon$} (i-12);
        \end{scope}
      \end{tikzpicture}
    }
  \end{center}

  \small

\begin{lstlisting}
\begin{scope}[$\dots$]
  $\dots\quad(\textsl{Wie vorher})$
  \draw[blue, <->] (i-2) --
    node[right] {$\text{\${}f(x\_0 + \textbackslash{}varepsilon  - f(x\_0) )\$}$} (i-12);
  \draw[blue, <->] (i-1) --
    node[below] {$\text{\${}\textbackslash{}varepsilon\$}$} (i-12);
\end{scope}
\end{lstlisting}

\end{frame}

\begin{frame}[t,fragile]

  \begin{center}
    \scalebox{0.5}{
      \begin{tikzpicture}
        \coordinate (O) at (0,0);
        \draw[->] (-0.3,0) -- (8,0) coordinate[label = {below:$x$}] (xmax);
        \draw[->] (0,-0.3) -- (0,5) coordinate[label = {right:$f(x)$}] (ymax);
        \path[name path=x] (0.3,0.5) -- (6.7,4.7);
        \path[name path=y] plot[smooth] coordinates {(-0.3,2) (2,1.5) (4,2.8) (6,5)};
        \begin{scope}[name intersections = {of = x and y, name = i}]
          \fill[gray!20] (i-1) -- (i-2 |- i-1) -- (i-2) -- cycle;
          \draw      (0.3,0.5) -- (6.7,4.7) node[pos=0.8, below right] {Sekante};
          \draw[red] plot[smooth] coordinates {(-0.3,2) (2,1.5) (4,2.8) (6,5)};
          \draw (i-1) node[label = {above:$P$}] {} -- node[left,yshift=-3pt]
          {$f(x_0)$} (i-1 |- O) node[label = {below:$x_0$}] {};
          \path (i-2) node[label = {above:$Q$}] {} -- (i-2 |- i-1) node (i-12) {};
          \draw (i-12) -- (i-12 |- O) node[label = {below:$x_0 + \varepsilon$}] {};
          \draw[blue, <->] (i-2) -- node[right] {$f(x_0 + \varepsilon) - f(x_0)$} (i-12);
          \draw[blue, <->] (i-1) -- node[below] {$\varepsilon$} (i-12);
          \path       (i-1 |- O) -- node[below] {$\varepsilon$} (i-2 |- O);
          \draw[gray]      (i-2) -- (i-2 -| xmax);
          \draw[gray, <->] ([xshift = -0.5cm]i-2 -| xmax) -- node[fill = white]
          {$f(x_0 + \varepsilon)$}  ([xshift = -0.5cm]xmax);
        \end{scope}
      \end{tikzpicture}
    }
  \end{center}

  \small

\begin{lstlisting}
\begin{scope}[$\dots$]
  $\dots\quad (\textsl{Wie vorher})$
  \path (i-1 |- O) --
    node[below] {$\text{\$\textbackslash{}varepsilon\$}$} (i-2 |- O);
  \draw[gray] (i-2) -- (i-2 -| xmax);
  \draw[gray, <->] ([xshift = -0.5cm]i-2 -| xmax) --
    node[fill = white] {$\text{\${}f(x\_0) + \textbackslash{}varepsilon\$}$}
    ([xshift = -0.5cm]xmax);
\end{scope}
\end{lstlisting}

\end{frame}

\begin{frame}[t,fragile]

  \begin{center}
    \scalebox{0.5}{
      \begin{tikzpicture}[
          thick,
          >=stealth',
          dot/.style = {
            draw,
            fill = white,
            circle,
            inner sep = 0pt,
            minimum size = 4pt
          }
        ]
        \coordinate (O) at (0,0);
        \draw[->] (-0.3,0) -- (8,0) coordinate[label = {below:$x$}] (xmax);
        \draw[->] (0,-0.3) -- (0,5) coordinate[label = {right:$f(x)$}] (ymax);
        \path[name path=x] (0.3,0.5) -- (6.7,4.7);
        \path[name path=y] plot[smooth] coordinates {(-0.3,2) (2,1.5) (4,2.8) (6,5)};
        \begin{scope}[name intersections = {of = x and y, name = i}]
          \fill[gray!20] (i-1) -- (i-2 |- i-1) -- (i-2) -- cycle;
          \draw      (0.3,0.5) -- (6.7,4.7) node[pos=0.8, below right] {Sekante};
          \draw[red] plot[smooth] coordinates {(-0.3,2) (2,1.5) (4,2.8) (6,5)};
          \draw (i-1) node[dot, label = {above:$P$}] (i-1) {} -- node[left,yshift=-3pt]
            {$f(x_0)$} (i-1 |- O) node[dot, label = {below:$x_0$}] {};
          \path (i-2) node[dot, label = {above:$Q$}] (i-2) {} -- (i-2 |- i-1)
            node[dot] (i-12) {};
          \draw           (i-12) -- (i-12 |- O) node[dot,
                                    label = {below:$x_0 + \varepsilon$}] {};
          \draw[blue, <->] (i-2) -- node[right] {$f(x_0 + \varepsilon) - f(x_0)$}
                                    (i-12);
          \draw[blue, <->] (i-1) -- node[below] {$\varepsilon$} (i-12);
          \path       (i-1 |- O) -- node[below] {$\varepsilon$} (i-2 |- O);
          \draw[gray]      (i-2) -- (i-2 -| xmax);
          \draw[gray, <->] ([xshift = -0.5cm]i-2 -| xmax) -- node[fill = white]
            {$f(x_0 + \varepsilon)$}  ([xshift = -0.5cm]xmax);
        \end{scope}
      \end{tikzpicture}
    }
  \end{center}

  \small

\begin{lstlisting}
\begin{tikzpicture}[thick, >=stealth',
    dot/.style = {
      draw,
      fill = white,
      circle,
      inner sep = 0pt,
      minimum size = 4pt
    }]
$\dots$
\end{lstlisting}
\end{frame}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% TeX-engine: luatex
%%% End:

